"use strict";
import express from 'express';
import logger from 'morgan';
import path from 'path';
import dotenv from 'dotenv';
import cors from 'cors';
dotenv.config();
console.log("HOST", process.env.DB_HOST)
import server from './server';
import db from './app/config/database';
const app = express();
app.use(cors())
app.use(logger('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use((req, res, next) => {
//   console.log(req.protocol + '://' + req.headers.host)
  process.env.URL = req.protocol + '://' + req.headers.host;
  console.log("Server start", process.env.URL);
  next();
})

require('./app/routes/index')(app, express);
require('./app/routes/users')(app, express);
// require('./app/routes/contacts')(app, express);
// require('./app/routes/messages')(app, express);
module.exports = app;
