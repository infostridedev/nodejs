"use strict";

const assert = require( "assert" );
const dotenv = require( "dotenv" );

// read in the .env file
dotenv.config();

// capture the environment variables the application needs
const { PORT,
   HOST,
   HOST_URL,
   COOKIE_ENCRYPT_PWD,
   DB_HOST,
   DB_NAME
} = process.env;



// validate the required configuration information
assert( PORT, "PORT configuration is required." );
assert( HOST, "HOST configuration is required." );
assert( HOST_URL, "HOST_URL configuration is required." );
assert( COOKIE_ENCRYPT_PWD, "COOKIE_ENCRYPT_PWD configuration is required." );
assert( DB_HOST, "DB_HOST is required." );
assert( DB_NAME, "DB_NAME configuration is required." );
// export the configuration information
module.exports = {
   port: PORT,
   host: HOST,
   url: HOST_URL,
   dbhost: DB_HOST,
   dbname: DB_NAME,
   cookiePwd: COOKIE_ENCRYPT_PWD,
};