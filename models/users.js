'use strict';
/**
 * User Model
 **/
import mongoose from 'mongoose';
const Schema = mongoose.Schema;
 // moment = require('moment');

 var usersSchema = new Schema({
  fullName    : String,
  socialType  : {
    type      : String,
    enum      : ['facebook', 'google', 'normal'],
    default   : 'normal'
  },
  socialId    : String,
  email       : {
    type      : String,
    lowercase : true
  },
  password    : String,
  alreadyExist: String,
  otp : Number,
  otpCreatedTime:Date,
  isVerified  : {
    type : Boolean,
    default : false
  },
  isDeleted   : {type : Boolean, default: false},
  userStatus  : {
    type      : String,
    enum      : ['enable', 'disable'],
    default   : 'enable'
  },
  createdAt   : {
    type      : Date,
    default   : Date.now()
  },
  updatedAt   :  {
    type      : Date,
    default   : Date.now()
  },
  userType    : {
    type      : String,
    enum      : ['admin', 'normal'],
    default   : 'normal'
  },
  token: String
});


module.exports = mongoose.model('users', usersSchema);