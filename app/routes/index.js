import ctrl from './../controller';
module.exports = (app, express) => {
    let router = express.Router();
    const userCtrl = ctrl.users;
    router.get('/', (req, res) => {
      res.send({"message" : "setup done with es6"})
    });
    router.get('/verifyEmailUrl/:token', userCtrl.verifyEmail)  
    app.use('/', router);
  }
  