import ctrl from './../controller';
import authApi from './../config/auth';
module.exports = (app, express) => {
    const userCtrl = ctrl.users;
    let router = express.Router();
    router.post('/signUp', userCtrl.registration);
    router.post('/login', userCtrl.loginUser);
    router.get('/verifyEmailUrl', userCtrl.verifyEmail);
    router.post('/forgotPasswordOtp', userCtrl.forgotPasswordOtp);
    router.put('/resetPassword', userCtrl.resetPassword);

    router.use(authApi.auth); //add auth middleware

    router.put('/updateProfile', userCtrl.updateProfile);
    router.get('/getProfile', userCtrl.getProfile);

    router.get('/admin/listofusers', userCtrl.listofusers);
    router.put('/admin/updateNormalUser', userCtrl.updateNormalUser)
    app.use('/users', router);
  }
  