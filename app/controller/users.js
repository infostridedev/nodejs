import Common from './../config/common';
import Users from './../../services/users'
import Email from './../config/emails/mailjet';

/****************************************************/
// Filename: user.js
// Change history:
// 04.01.2020
/****************************************************/

/* registration of end user 04.01.2020 */
exports.registration = async (req, res) => {
  console.log("Inside", req.body);
  
  const payloadData = req.body;
  let userInfo = {};
    userInfo.fullName = payloadData.fullName;
    userInfo.socialType = 'normal';
    userInfo.email = payloadData.email;
    // userInfo.otp = "";
  switch (userInfo.socialType) {
    case 'normal':
      Users.findusers(1, {"email": payloadData.email, 'socialType': 'normal'}, async (err, userObj)=> {
        if(userObj){
          if(userObj.isVerified == true){
            return res.send(Common.errorResponse("forbidden", "Email is already exists, please provide another user email!"));
          }else {
            let tokenData = {
              name: userObj.fullName,
              id: userObj._id,
              tokenTime:new Date()
            };
            userObj.token = Common.getToken(tokenData)
            const email = new Email(payloadData.email, payloadData.fullName, 'Confirmation Link', userObj);
            await email.registration();
            return res.send(Common.errorResponse("forbidden", "Your account is not verified!"));
          }
        }else {
          userInfo.password = Common.encrypt(payloadData.password);
          Users.createuser(userInfo, async (err, userObj)=> {
            if (!err) {
                var tokenData = {
                    name: userObj.fullName,
                    id: userObj._id,
                    tokenTime:new Date()
                };
                userObj.token = Common.getToken(tokenData)
                const email = new Email(payloadData.email, payloadData.fullName, 'Confirmation Link', userObj);
                await email.registration();
                res.send(Common.successResponse('Please confirm your email id by clicking on link in email', userObj));
            } else {
                if (11000 === err.code || 11001 === err.code) {
                    res.send(Common.errorResponse("forbidden", "Please provide another user email"));
                } else res.send(Common.errorResponse("forbidden", err)); // HTTP 403
            }
          })
        }
    });
    break;
  }
}


/****************************************************/
// Filename: user.js
// Change history:
// 04.01.2020 
// /****************************************************/

/* login of end user 04.01.2020 */
exports.loginUser = (req, res) => {
  Users.findusers(1, {'email': req.body.email, 'socialType': 'normal'}, (err, userObj)=> {
    // console.log(err, userObj);
    // console.log('----------', req.body.password, Common.decrypt(userObj.password));
      if (!err) {
          if (userObj === null) return res.send(Common.errorResponse("forbidden", "Invalid username or password"));
          if (req.body.password === Common.decrypt(userObj.password)) {
            if(userObj.isVerified == false){
              var tokenData = {
                name: userObj.fullName,
                id: userObj._id,
                tokenTime:new Date()
              };
              userObj.token = Common.getToken(tokenData)
              return res.send(Common.errorResponse("forbidden", 'Your email is not confirmed'));
            }else {
              var tokenData = {
                name: userObj.fullName,
                id: userObj._id,
                userType: userObj.userType,
                tokenTime:new Date()
              };
              userObj.token = Common.getToken(tokenData)
              userObj = Common.customeResponse(['createdAt', 'updatedAt','__v'], userObj)
              return res.send(Common.successResponse('Successfully authenticated!', userObj));
            }

          } else res.send(Common.errorResponse("forbidden", "Invalid username or password."));
      } else {
          if (11000 === err.code || 11001 === err.code) {
              return res.send(Common.errorResponse("forbidden", "Please provide another user email"));
          } else {
              console.error(err);
              return res.send(Common.errorResponse("badImplementation", err));
          }
      }
  });
}


// /****************************************************/
// // Filename: user.js
// // Change history:
// // 08.01.2020 
// /****************************************************/
//
// /* email verificatiaon of end user 08.01.2020 */
exports.verifyEmail = (req, res) => {
  if(req.params.token){
    var userId = Common.getUserId(req.params.token);
    var tokenTime = Common.getJwtDecode(req.params.token).tokenTime;
    var timeDiff =  new Date() - new Date(tokenTime);
    var minutes = Math.floor(timeDiff / 60000);
    if(minutes>30){
        return res.send(Common.createdResponse("Your link has been expired."));
    }
        var query = {
          _id : userId
        }
        Users.findusers(1, query, (err, user) =>{
            if (err) {
                console.error(err);
                return res.send(Common.errorResponse("badImplementation", err));
            }
            if (user === null) return res.send(Common.errorResponse("forbidden", "Invalid verification link"));
            if (user.isVerified === true){
                return res.send(Common.errorResponse("forbidden", "Account is already verified"));
            }
            user.isVerified = true;
            Users.updateUser(query, user, {}, async (err, user)=>{
                if (err) {
                    return res.send(Common.errorResponse("badImplementation", err));
                }
                return res.send(Common.createdResponse("Your "+process.env.APP_NAME+" account is sucessfully verified."));
            })
        })
    } else {
          return res.send(Common.errorResponse("badImplementation", 'No token exist'));
    }
}


// /****************************************************/
// // Filename: user.js
// // Change history:
// // 10.01.2020 
// /****************************************************/

// /* forgot password for end user 10.01.2020*/
exports.forgotPasswordOtp = (req, res) => {
  // console.log(req.body);
  var email = (req.body.email).toLowerCase();
  Users.findusers(1, {'email': email, 'socialType': 'normal'}, async(err, userObj) => {
      if (!err) {
          if (userObj === null) return res.send(Common.errorResponse("forbidden", "invalid number"));
          if(!userObj.isVerified) return res.send(Common.errorResponse("forbidden", "email not verified yet"));
          //if(userObj.isSuspended == true) return res.json({ statusCode: 503, message: 'Your account has been suspended, please contact admin for this.'});

          userObj.otp = Common.randomNumber(4);
          const email = new Email(userObj.email, userObj.fullName, 'Forgot Password', userObj);
          await email.forgotPasswordOtp();
          var query = {"_id":userObj._id};
          userObj.otpCreatedTime = new Date()
          Users.updateUser(query, userObj, {}, (err, user) => {
              if (err) {
                  // console.error(err);
                  return res.send(Common.errorResponse("badImplementation", err));
              }else{
                var obj = {"_id":userObj.id}
                return res.send(Common.successResponse('OTP has sent successfully to registered email id!',obj));
              }
          });
      } else {
          // console.error(err);
          return res.send(Common.errorResponse("badImplementation", err));
      }
  });
};

/****************************************************/
// Filename: user.js
// Change history:
// 10.01.2020 /
// Description : Reset Password
/****************************************************/
exports.resetPassword = (req, res) =>{
  let payloadData = req.body;
  let userId = req.headers['x-access-token'] ? Common.getUserId(req.headers['x-access-token']) : payloadData.userId;
  if(payloadData.password == "" || !payloadData.password) return res.send(Common.errorResponse("forbidden", "Password cannot be empty"))
  Users.findusers(1, {_id : userId}, async(err, userObj) => {
      if (!err) {
       if (userObj === null) return res.send(Common.errorResponse("forbidden", "Sorry no record found"));
       userObj.password = Common.encrypt(payloadData.password);
       if(payloadData.otp){
         let otpCreatedTime = userObj.otpCreatedTime;
         let timeDiff =  new Date() - new Date(Number(otpCreatedTime));
         //console.log("timeDiff", timeDiff, otpCreatedTime, new Date(otpCreatedTime), typeof(otpCreatedTime))
          let minutes = Math.floor(timeDiff / 60000);
          //console.log("minutes", minutes, payloadData.otp, userObj.otp)
          if(minutes>30){
            return res.send(Common.createdResponse("Your OTP is either expired or invalid.Please send again."));
          }else{
           if(payloadData.otp == userObj.otp){
             userObj.otp = "",
             userObj.otpCreatedTime="";
           }else{
             return res.send(Common.createdResponse("Your OTP is either expired or invalid.Please send again."));
           }
          }
       }else{
         if(payloadData.oldPassword == "" || !payloadData.oldPassword) return res.send(Common.errorResponse("forbidden", "Old password cannot be empty"))
         if(Common.encrypt(payloadData.oldPassword) != userObj.password){
           return res.send(Common.errorResponse("badImplementation","Incorrect old password."))
         }
       }
       Users.updateUser({_id : userId}, {otpCreatedTime: "", otp: "", password: userObj.password}, {}, (err, updatedData) => {
         if(!err){
           return res.send(Common.successResponse('Password updated successfully', userObj));
         }else{
           res.send(Common.errorResponse("forbidden", err)); // HTTP 403
         }  
       });
      }else {
          return res.send(Common.errorResponse("badImplementation", err));
      }
  })
}


// /****************************************************/
// // Filename: user.js
// // Change history:
// // 10.01.2020  
// /****************************************************/
//
// /* update profile of end user 10.01.2020 */

exports.updateProfile = (req, res) => {
    let payloadData = req.body;
    let userId = Common.getUserId(req.headers['x-access-token']);
    Users.findusers(1, {_id : userId}, async(err, userObj) => {
      if(err){
        return res.send(Common.errorResponse("badImplementation", err))
      }
      else if(userObj == null){
        return res.send(Common.errorResponse("notFound", 'User not found'))
      } else {
        let dataToUpdate = {};
        for (const prop in payloadData) {
          console.log(`obj.${prop} = ${payloadData[prop]}`);
          if(prop == 'password'){
            let pasw = Common.encrypt(payloadData[prop]);
            dataToUpdate.password = pasw
          }else{
            dataToUpdate[prop] = payloadData[prop]
          }
        }
        Users.updateUser({_id : userId}, dataToUpdate, {new: true}, (err, updatedData) => {
          if(!err){
            return res.send(Common.successResponse('Profile updated successfully', updatedData));
          }else{
            res.send(Common.errorResponse("forbidden", err)); // HTTP 403
          }  
        });
        }
    });
}

exports.getProfile = async (req, res) => {
  let payloadData = req.query;
  let userId = Common.getUserId(req.headers['x-access-token']);
  if(payloadData.userId){
    userId = payloadData.userId;
  }
  Users.findusers(1, {_id : userId}, async(err, userObj) => {
    if(!err){
      if(userObj == null) return res.send(Common.errorResponse("notFound", 'User not found'))
      return res.send(Common.successResponse('Success', userObj));
    }else{
      return res.send(Common.errorResponse("badImplementation", err));
    }
  });
}


//only for admin
exports.listofusers = async (req, res) => {
  let payloadData = req.query;
  let userId = Common.getUserId(req.headers['x-access-token']);
  if(payloadData.userId){
    userId = payloadData.userId;
  }
  Users.findusers(null, {userType : 'normal'}, async(err, userObj) => {
    if(!err){
      if(userObj == null) return res.send(Common.errorResponse("notFound", 'User not found'))
      return res.send(Common.successResponse('Success', userObj));
    }else{
      return res.send(Common.errorResponse("badImplementation", err));
    }
  });
}

exports.updateNormalUser = async (req, res) => {
  let payloadData = req.body;
    let userId = payloadData.userId;
    Users.findusers(1, {_id : userId}, async(err, userObj) => {
      if(err){
        return res.send(Common.errorResponse("badImplementation", err))
      }
      else if(userObj == null){
        return res.send(Common.errorResponse("notFound", 'User not found'))
      } else {
        let dataToUpdate = {};
        for (const prop in payloadData) {
          console.log(`obj.${prop} = ${payloadData[prop]}`);
          if(prop == 'password'){
            let pasw = Common.encrypt(payloadData[prop]);
            dataToUpdate.password = pasw
          }else{
            dataToUpdate[prop] = payloadData[prop]
          }
        }
        Users.updateUser({_id : userId}, dataToUpdate, {new: true}, (err, updatedData) => {
          if(!err){
            return res.send(Common.successResponse('User updated successfully', updatedData));
          }else{
            res.send(Common.errorResponse("forbidden", err)); // HTTP 403
          }  
        });
        }
    });
}