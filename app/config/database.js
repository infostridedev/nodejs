import Mongoose from "mongoose";
import config from "./../../config";
// console.log('DB_HOST----------', process.env.DB_HOST, 'DB_NAME', process.env.DB_NAME)
//add database
Mongoose.connect('mongodb://'+config.dbhost+'/'+config.dbname);
let db = Mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function callback() {
    console.log('Connection with database succeeded.');
});

module.exports = db;


