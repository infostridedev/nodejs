"use strict";
import colors, { black } from 'colors';
module.exports = {
	/**
	 * Log messages to console
	 * @param {string} args pass multiple messages 
	 */
	log : (args) => {
        console.log(' Logs Start: '.bgBlue, '\n');
            console.log('\t', args, '\n');
        console.log(' Logs End: '.bgBlue, '\n');
    },

	/**
	 * Log information to console
	 * @param {string} args pass multiple messages
	 */
	info : (args) => {
		console.log(' Info Start: '.black.bgYellow, '\n');
            console.info('\t', args, '\n');
        console.log(' Info End: '.black.bgYellow, '\n');
    },
	
	/**
	 * Log error messages to console
	 * @param {string} args pass multiple error messages
	 */
	error : (args) => {
        console.log(' Error Start: '.black.bgRed, '\n');
            console.info('\t', args, '\n');
        console.log(' Error End: '.black.bgRed, '\n');
    },

    custom : (args) => {
        console.info(args.bgBlue, '\n');
    }
    
}