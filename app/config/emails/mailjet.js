/**
 *
 * This call sends a message to the given recipient with vars and custom vars.
 *
 */
// import mailjet from 'node-mailjet'; // not supported in es6 yet
const mailjet = require('node-mailjet').connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE);
module.exports = class SendEmail{
    constructor(emailTo, name, subject, variables){
        this.emailTo = emailTo;
        this.name = name;
        this.subject = subject;
        this.variables = variables;
        this.confirmation_link = process.env.URL+'/verifyEmailUrl/'+variables.token;
    }

    registration(){
        console.log("this id", this.emailTo);
        return new Promise((resolve, rejects)=>{
            const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({      
                "Messages":[
                    {
                        "From": {
                            "Email": process.env.FROM_EMAIL,
                            "Name": process.env.APPNAME
                        },
                        "To": [
                            {
                                "Email": this.emailTo,
                                "Name": this.name
                            }
                        ],
                        "TemplateID": 1164646,
                        "TemplateLanguage": true,
                        "Subject": this.subject,
                        "Variables": {
                            confirmation_link : this.confirmation_link,
                            app_name: process.env.APPNAME
                        }
                    }
                ]
            })
        request
            .then((result) => {
                console.log(result.body)
                resolve(true)
            })
            .catch((err) => {
                console.log(err.statusCode)
                resolve(err.statusCode)
            })
        })
    }

    forgotPasswordOtp(){
        console.log("this id", this.emailTo);
        return new Promise((resolve, rejects)=>{
            const request = mailjet
            .post("send", {'version': 'v3.1'})
            .request({      
                "Messages":[
                    {
                        "From": {
                            "Email": process.env.FROM_EMAIL,
                            "Name": process.env.APPNAME
                        },
                        "To": [
                            {
                                "Email": this.emailTo,
                                "Name": this.name
                            }
                        ],
                        "TemplateID": 1167762,
                        "TemplateLanguage": true,
                        "Subject": this.subject,
                        "Variables": {
                            otp : this.variables.otp,
                            full_name: this.fullName,
                            app_name: process.env.APPNAME
                        }
                    }
                ]
            })
        request
            .then((result) => {
                console.log(result.body)
                resolve(true)
            })
            .catch((err) => {
                console.log(err.statusCode)
                resolve(err.statusCode)
            })
        })
    }
}
