const nodemailer = require("nodemailer"),
    crypto = require('crypto'),
    jwt = require('jsonwebtoken'),
    config = require('./../../config'),
    algorithm = 'aes-256-ctr';

const privateKey = process.env.PWD_KEY;
// console.log('privateKey', privateKey)


exports.decrypt = (password) => {
    let decipher = crypto.createDecipher(algorithm, privateKey);
    let dec = decipher.update(password, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
};

exports.encrypt = (password) => {
  let cipher = crypto.createCipher(algorithm, privateKey);
  let crypted = cipher.update(password, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
};


// method to decrypt data(password)
// decryptData = (password) => {
//     var decipher = crypto.createDecipher(algorithm, privateKey);
//     var dec = decipher.update(password, 'hex', 'utf8');
//     dec += decipher.final('utf8');
//     return dec;
// }

// // method to encrypt data(password)
// encrypt = (password) => {
//     var cipher = crypto.createCipher(algorithm, privateKey);
//     var crypted = cipher.update(password, 'utf8', 'hex');
//     crypted += cipher.final('hex');
//     return crypted;
// }

exports.randomNumber = (len) => {
    let text = "";
    let charset = "123456789";
    for( let i=0; i < len; i++ )
        text += charset.charAt(Math.floor(Math.random() * charset.length));
        return text;
};

exports.getToken = (tokenData) => {
    let secretKey = process.env.JWT || 'jwtkey';
    return jwt.sign(tokenData, secretKey, {expiresIn: '18h'});
}
exports.getUserId = (token) => {
    let secretKey = process.env.JWT || 'jwtkey';
    let tokenDecode = jwt.verify(token, secretKey);
    return tokenDecode.id;
}

exports.getJwtDecode = (token) => {
    let secretKey = process.env.JWT || 'jwtkey';
    let tokenDecode = jwt.verify(token, secretKey);
    return tokenDecode;
}

exports.successResponse = (message, data) => {
    return {
        "statusCode": 200,
        "data": data,
        "message": message
    }
}

exports.successResponseAdded = (message, data, otherInfo) => {
  let respone = {
    "statusCode": 200,
      "data": data,
      "message": message,
      "otherInfo" : otherInfo
  };
  return respone;
}

exports.createdResponse = (message, data) => {
    return {
        "statusCode": 201,
        "data": data,
        "message": message
    }
}

exports.customeResponse = (arr, obj, isArray) => {
  let newObj = JSON.parse(JSON.stringify(obj));
    arr.forEach(function(key) {
      if(isArray){
        for (const newkey in newObj) {
          if (newObj.hasOwnProperty(newkey)) {
            const element = newObj[newkey];
            delete newObj[newkey][key];
          }
        }
      }else{
        delete newObj[key];
      }
    });
    return newObj;
}


exports.errorResponse = (type, message) =>{
  switch (type) {
    case 'badRequest':
      return {
        "statusCode": 400,
        "error": "Bad Request",
        "message": message
      }
      break;
    case 'unauthorized':
      return {
        "statusCode": 401,
        "error": "Unauthorized",
        "message": message
      };
      break;
    case 'forbidden':
      return {
          "statusCode": 403,
          "error": "Forbidden",
          "message": message
      };
      break;
    case 'notFound':
      return {
          "statusCode": 404,
          "error": "Not Found",
          "message": message
      };
      break;
    case 'badImplementation':
      return {
          "statusCode": 500,
          "error": "Internal Server Error",
          "message": message
      }
      break;
    default:
  }
}




exports.sendTextMessage = (phoneNumber, otp) => {
  return new Promise((resolve, reject)=> {
    console.log(phoneNumber, otp);
    
    const accountSid = config.accountSid;
    const authToken = config.authToken;
    const client = require('twilio')(accountSid, authToken);
    console.log(accountSid, authToken);
    client.messages
      .create({
        body: `${otp} is OTP to access your Qchat account. OTP is valid for 30 min. Do not share your OTP with anyone.`,
        from: config.phoneFrom,
        to: phoneNumber
      })
    .then((message) => {
      console.log(message),
      resolve(message)
    } 
    )
    .catch((err) => {
      console.log(err),
      resolve(err)
    } 
    );
  })
}
